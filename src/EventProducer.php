<?php
/**
 * @author Alexander Stepanenko <alex.stepanenko@gmail.com>
 */

namespace Sdk\Events;

use GuzzleHttp\Client;
use indigerd\oauth2\authfilter\Module as AuthFilter;
use Sdk\Events\Amqp\Amqp;

class EventProducer implements EventProducerInterface
{
    protected $client;

    protected $eventApiUrl;

    protected $authFilter;

    protected $tokenData = [];

    public function __construct(Client $client, AuthFilter $authFilter, string $eventApiUrl)
    {
        $this->client = $client;
        $this->authFilter = $authFilter;
        $this->eventApiUrl = \rtrim($eventApiUrl, '/');
    }

    protected function getClientAccessToken() : string
    {
        if (empty($this->tokenData) || (\time() + 600 > $this->tokenData['expire_time'])) {
            $this->tokenData = $this->createAccessToken();
        }
        return $this->tokenData['access_token'];
    }

    protected function createAccessToken() : array
    {
        $responseData = $this->authFilter->requestAccessToken(
            '',
            '',
            'events',
            false,
            'client_credentials'
        );
        if (empty($responseData['access_token'])) {
            throw new \RuntimeException('Cannot obtain access token');
        }
        $responseData['expire_time'] = \time() + $responseData['expires_in'];
        return $responseData;
    }

    public function fireEvent(Arrayable $entity, $eventName, string $eventVersion = 'v1') : void
    {
        $this->client->post(
            $this->eventApiUrl . '/events',
            [
                'form_params' => [
                    'name' => $eventName,
                    'data' => $entity->toArray(),
                    'version' => $eventVersion
                ],
                'headers' => [
                    'Authorization' => $this->getClientAccessToken()
                ],
            ]
        );
    }

    public function fireEventDelayed(Amqp $amqp, Arrayable $entity, $eventName, string $eventVersion = 'v1') : void
    {
        $amqp->pushToQueue([
            'entity' => $entity->toArray(),
            'eventName' => $eventName,
            'eventVersion' => $eventVersion
        ]);
    }
}
