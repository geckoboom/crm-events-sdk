<?php
/**
 * @author Alexander Stepanenko <alex.stepanenko@gmail.com>
 */

namespace Sdk\Events;

use Sdk\Events\Amqp\Amqp;

interface EventProducerInterface
{
    public function fireEvent(Arrayable $entity, $eventName, string $eventVersion = 'v1') : void;

    public function fireEventDelayed(Amqp $amqp, Arrayable $entity, $eventName, string $eventVersion = 'v1') : void;
}
