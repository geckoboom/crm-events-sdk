<?php
/**
 * @author Alexander Stepanenko <alex.stepanenko@gmail.com>
 */

namespace Sdk\Events\Amqp;

use devyk\amqp\components\Amqp;

class AmqpClientFactory
{
    protected $config;

    /**
     * AmqpClientFactory constructor.
     * @param $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @return Amqp
     */
    public function create() : Amqp
    {
        return new Amqp($this->config);
    }
}
