<?php
/**
 * @author Alexander Stepanenko <alex.stepanenko@gmail.com>
 */

namespace Sdk\Events;

interface Arrayable
{
    public function toArray(): array;
}
