<?php
/**
 * @author Alexander Stepanenko <alex.stepanenko@gmail.com>
 */

namespace Sdk\Events;

use GuzzleHttp\Client;
use indigerd\oauth2\authfilter\Module as AuthFilter;

class EventSubscriber implements EventSubscriberInterface
{
    protected $client;

    protected $authFilter;

    protected $eventApiUrl;

    protected $apiUrl;

    public function __construct(Client $client, AuthFilter $authFilter, string $eventApiUrl, string $apiUrl)
    {
        $this->client = $client;
        $this->authFilter = $authFilter;
        $this->eventApiUrl = \rtrim($eventApiUrl, '/');
        $this->apiUrl = \rtrim($apiUrl, '/');
    }

    protected function getAccessToken() : array
    {
        $token = $this->authFilter->requestAccessToken(
            '',
            '',
            'event-subscriptions',
            false,
            'client_credentials'
        );
        if (empty($token['access_token'])) {
            throw new \RuntimeException('Cannot obtain access token');
        }
        return $token;
    }

    public function subscribe(
        string $eventName,
        string $endpoint,
        string $httpMethod,
        string $eventVersion = 'v1'
    ) : void {
        $token = $this->getAccessToken();
        $this->client->post(
            $this->eventApiUrl . '/event-subscriptions',
            [
                'form_params' => [
                    'event' => $eventName,
                    'endpoint' => $this->apiUrl . '/' . \trim($endpoint, '/'),
                    'method' => $httpMethod,
                    'version' => $eventVersion
                ],
                'headers' => [
                    'Authorization' => $token['access_token'],
                    'Accept' => 'application/json'
                ]
            ]
        );
    }

    public function unSubscribe(
        string $eventName,
        string $endpoint,
        string $httpMethod,
        string $eventVersion = 'v1'
    ) : void {
        // URL: /event-subscriptions/:event/:version/:method/:endpoint
        $token = $this->getAccessToken();
        $url = $this->eventApiUrl
            . '/event-subscriptions/'
            . $eventName . '/'
            . $eventVersion . '/'
            . $httpMethod . '/'
            . \urlencode($this->apiUrl . '/' . \trim($endpoint, '/'));

        $this->client->delete(
            $url,
            [
                'headers' => [
                    'Authorization' => $token['access_token']
                ]
            ]
        );
    }
}
