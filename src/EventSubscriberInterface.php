<?php
/**
 * @author Alexander Stepanenko <alex.stepanenko@gmail.com>
 */

namespace Sdk\Events;

interface EventSubscriberInterface
{
    public function subscribe(
        string $eventName,
        string $endpoint,
        string $httpMethod,
        string $eventVersion = 'v1'
    ) : void;

    public function unSubscribe(
        string $eventName,
        string $endpoint,
        string $httpMethod,
        string $eventVersion = 'v1'
    ) : void;
}
